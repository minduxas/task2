var people = [
    {gender:"Male", name:"Jonas", surname:"Jonaitis", birthdate:"2010.10.10", email:"jonas@gmail.com", city:"Vilnius"},
    {gender:"Male", name:"Petras", surname:"Petraitis", birthdate:"2010.10.10", email:"petras@gmail.com", city:"Moletai"},
    {gender:"Female", name:"Ona", surname:"Onaite", birthdate:"2002.02.02", email:"ona@gmail.com", city:"Utena"},
    {gender:"Female", name:"Dovile", surname:"Dovilaite", birthdate:"2009.09.09", email:"jonas@gmail.com", city:"Kaunas"},
    {gender:"Female", name:"Jurga", surname:"Jurgaite", birthdate:"2001.01.01", email:"jonas@gmail.com", city:"Palanga"}
];

people.sort(function (a, b) {
    return (
        b.gender.localeCompare(a.gender) ||
        Date.parse(b.birthdate) - Date.parse(a.birthdate) ||
        a.name.localeCompare(b.name)
    );
});

console.log (people);





